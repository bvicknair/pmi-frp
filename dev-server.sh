#!/usr/bin/bash

# This stopped being useful the moment we had to deal with CORS.  At that point
# I installed Apache so that my API and these HTML/js files could be served
# from the same "origin", to avoid CORS problems.  At that point, I stopped
# needing python's SimpleHTTPServer

pulp browserify --to js/Main.js && python2 -m SimpleHTTPServer 8000
