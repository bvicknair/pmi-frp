-- |A Stage holds Edits that have yet to be committed.
module Stage
    ( Stage
    , Edit(..)
    , Weld(..)
    , edits
    , empty
    , append
    ) where


------------------------------------------------------------------------------
import Data.Array as Array
import Id ( Unique )
import Pmi.Lang.V3 as Lang
import Pmi.Lang.Types ( ProjectName
                      , WeldMaterial
                      , WeldSize
                      )
------------------------------------------------------------------------------



-- |A Stage holds Edits that have yet to be committed.
newtype Stage = Stage { edits :: Array Edit }

-- |Get the edits in a Stage, newest edits ordered before older edits.
edits :: Stage -> Array Edit
edits (Stage s) = s.edits

empty :: Stage
empty = Stage { edits : [] }

-- |Append an edit to the a Stage, newest edits are ordered before older edits.
append :: Edit -> Stage -> Stage
append e (Stage s) = Stage s{edits=Array.cons e s.edits}

-- |The values the UI can create to mutate the "current view" of a project.
-- Besides Core, all other expressions are project-specific, i.e. they don't
-- include a project id field because it is assumed the expression is "about"
-- the "active" project in the UI, of which there is only one.
data Edit = Core Lang.Expr -- We can say the core expressions too.
          | SetProjectName ProjectName -- The "active" project.
          | AddWeld (Unique Weld)



newtype Weld = Weld { mat  :: WeldMaterial
                    , size :: WeldSize
                    }
