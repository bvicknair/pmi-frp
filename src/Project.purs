module Project
    ( Project
    , Log

      -- Create new from log.
    , project

      -- View current state
    , id
    , index
    , log
    , name
    , stagedExprs

      -- Misc.
    , setId

      -- Mutate "current view"
    , appendEdit
    ) where


------------------------------------------------------------------------------
import Data.Array ( concat )
import Data.Functor ( map )
import Data.Maybe ( Maybe )
import Data.Monoid ( (<>) )
import Data.Show ( class Show
                 , show
                 )
import Id ( Id
          , Unique(Unique)
          )
import Index ( Index )
import Index as Index
import Pmi.Lang.Types ( ProjectName )
import Pmi.Lang.V3 ( Commit )
import Prelude ( ($) )
import Stage ( Edit( AddWeld
                   , Core
                   , SetProjectName
                   )
             , Stage
             , Weld(Weld)
             )
import Stage as Stage
------------------------------------------------------------------------------

newtype Project = Project { id    :: Id
                          , log   :: Log
                          , index :: Index
                          , stage :: Stage
                          }

instance showProject :: Show Project where
    show (Project p) = show p.id <> ": " <> show p.log


project :: Id -> Log -> Project
project id' log' = Project { id    : id'
                           , log   : log'
                           , index : Index.foldlLog Index.empty log'
                           , stage : Stage.empty
                           }

id :: Project -> Id
id (Project p) = p.id

log :: Project -> Log
log (Project p) = p.log

index :: Project -> Index
index (Project p) = p.index

name :: Project -> Maybe ProjectName
name (Project p) = Index.projectName p.id p.index

setId :: Id -> Project -> Project
setId id' (Project p) = Project p{id=id'}


-- |Uncommitted Index.Expr.
--
-- Sometimes we want to see the list of Edits, but sometimes we want to see
-- that list of Edits as a list of Index.Expr, for example, when saving.
stagedExprs :: Project -> Array Index.Expr
stagedExprs proj@(Project p) = concat $ map (editToExprs proj) edits
    where edits = Stage.edits p.stage

appendEdit :: Edit -> Project -> Project
appendEdit e proj@(Project p) =  Project p{ index = newIndex
                                          , stage = newStage
                                          }
  where newStage = Stage.append e p.stage
        newIndex = Index.foldrUncommitted p.index (editToExprs proj e)

-- |UI notion of "edit" to instructions to "mutate" and index.
editToExprs :: Project -> Edit -> Array Index.Expr
editToExprs _ (Core e) = [Index.liftExpr e]
editToExprs (Project p) (SetProjectName n) = [Index.ProjectNamed p.id n]
editToExprs (Project p) (AddWeld (Unique wid (Weld w))) =
    [ Index.PipeWeldOnProject wid p.id
    , Index.PipeWeldHasSize wid w.size
    , Index.PipeWeldHasMaterial wid w.mat
    ]


type Log = Array Commit
