module Error
    ( Error(..)
    , JSONParseError
    , InvalidJSON
    , prettyError
    , liftJSON
    ) where

------------------------------------------------------------------------------
import Control.Monad.Eff.Exception as EffException
import Data.Monoid ( (<>) )
import Network.HTTP.StatusCode ( StatusCode(..) )
import Pmi.Lang.V3 ( JSONError(..) )
import Prelude ( show )
------------------------------------------------------------------------------


data Error = -- |Usually a problem communicating with server at all: no
             -- internet connection, malformed URL, broken socket etc.
             AjaxError EffException.Error
           | BadLogin
             -- |TODO: Eventually we should have an Error constructor for every
             -- possible error received from the server, and then one catchall
             -- for unexpected errors.
           | BadRequest StatusCode String
           | BadJSON JSONParseError InvalidJSON
           | UnexpectedStatusCode StatusCode String

type JSONParseError = String

-- |A string encoded in JSON, but invalid data for a particular case.
type InvalidJSON = String


-- |Pretty error for the user.
prettyError :: Error -> String
prettyError (AjaxError e) = EffException.message e
prettyError (BadRequest _ m) = m
prettyError BadLogin = "Bad username or password"
prettyError (BadJSON _ _) = "Invalid JSON data, call an admin"
prettyError (UnexpectedStatusCode s m) = "Unexpected status code"
                                      <> ", call an admin."
                                      <> " CODE=" <> show s
                                      <> ", BODY=" <> show m


-- |Lift a JSONError response from the server to our app error type.
liftJSON :: JSONError -> Error
liftJSON (JSONError j) = BadRequest code title
    where code  = (StatusCode j.statusCode)
          title = j.title
-- TODO: Inspect j.statusCode and create an Error constructor for each unique
-- error from server.
