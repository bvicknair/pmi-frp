module Main where

import Control.Monad.Eff (Eff)
import Control.Monad.Eff.Exception ( EXCEPTION )
import Data.UUID ( GENUUID )
import Foldp ( foldp )
import Id ( Id(Local) )
import Network.HTTP.Affjax ( AJAX )
import Prelude ( Unit
               , bind
               )
import Project ( project )
import Pux ( start )
import Pux.Renderer.React ( renderToDOM )
import Signal.Channel ( CHANNEL )
import State as State
import UI ( view )

main :: Eff ( channel   :: CHANNEL
            , exception :: EXCEPTION
            , ajax      :: AJAX
            , uuid    :: GENUUID
            ) Unit
main = do
  app <- start config
  renderToDOM "#main" app.markup app.input
  where config = { initialState : State.initial newProj
                 , view         : view
                 , foldp        : foldp
                 , inputs       : []
                 }
        newProj = project (Local 0) []

