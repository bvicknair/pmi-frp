module State
    ( State(..)
    , initial

    , busy
    , setBusy
    , notBusy

    , proj
    , setProj

    , err
    , setErr
    , clearErr

    , user
    , setUser

    , appendEdit

    , projList
    , setProjList

    , nextLocalId
    , setNextLocalId

    , authForm
    , setAuthForm

    , tree
    , setTree
    , setSpec
    , specInput
    , setSpecInput
    , selectedPaths
    , selectedTreeObjs
    ) where

------------------------------------------------------------------------------
import Data.List ( List )
import Data.Maybe ( Maybe(Just, Nothing) )
import Error ( Error )
import Index as Index
import Pmi.Lang.V3.Db.Project as DbProj
import Prelude ( ($)
               , (<<<)
               )
import Project ( Project )
import Project as Project
import Stage ( Edit )
import Types ( User )
import UI.Auth as Auth
import UI.Tree as Tree
import UI.TreeTypes as TreeTypes
------------------------------------------------------------------------------


newtype State = State { -- |Just User if logged in
                        user :: Maybe User

                        -- |UI State if Auth form is showing.
                      , authForm :: Maybe Auth.State

                        -- |There is always an "active" project, even if that
                        -- means it is an empty "new" project.
                      , proj  :: Project

                        -- |List of existing projects to choose from when
                        -- loading a project.
                      , projList :: Array DbProj.Listed

                        -- |A modal message while doing HTTP etc.
                      , busy  :: Maybe String
                      , err   :: Maybe Error

                        -- |The next locally unique id.  The top foldp uses
                        -- this field to remember which Id to start the next
                        -- LocalId Monad action with.
                      , nextLocalId :: Int

                      , tree :: Tree.State

                        -- |The text input that allows editing of the Tree.Spec
                        -- as a list of comma-separated attribute names.
                        , specInput :: String
                      }





initial :: Project -> State
initial p = State { user        : Nothing
                  , authForm    : Just $ Auth.init Nothing
                  , proj        : p
                  , projList    : []
                  , busy        : Nothing
                  , err         : Nothing
                  , nextLocalId : 0
                  , tree        : Tree.init
                  , specInput   : ""
                  }

busy :: State -> Maybe String
busy (State s) = s.busy

setBusy :: String -> State -> State
setBusy m (State s) = State s{busy=Just m}

notBusy :: State -> State
notBusy (State s) = State s{busy=Nothing}

proj :: State -> Project
proj (State s) = s.proj

setProj :: Project -> State -> State
setProj p (State s) = State s{ proj  = p }

err :: State -> Maybe Error
err (State s) = s.err

setErr :: Error -> State -> State
setErr e (State s) = State s{err=Just e}

clearErr :: State -> State
clearErr (State s) = State s{err=Nothing}

user :: State -> Maybe User
user (State s) = s.user

setUser :: Maybe User -> State -> State
setUser u (State s) = State s{user=u}

projList :: State -> Array DbProj.Listed
projList (State s) = s.projList

setProjList :: Array DbProj.Listed -> State -> State
setProjList ps (State s) = State s{projList=ps}

appendEdit :: Edit -> State -> State
appendEdit e st@(State s) = State s{proj=Project.appendEdit e s.proj}

nextLocalId :: State -> Int
nextLocalId (State s) = s.nextLocalId

setNextLocalId :: Int -> State -> State
setNextLocalId i (State s) = State s{nextLocalId=i}

authForm :: State -> Maybe Auth.State
authForm (State s) = s.authForm

setAuthForm :: Maybe Auth.State -> State -> State
setAuthForm auth (State s) = State s{authForm=auth}

tree :: State -> Tree.State
tree (State s) = s.tree

setTree :: Tree.State -> State -> State
setTree t (State s) = State s{tree=t}

setSpec :: TreeTypes.Spec -> State -> State
setSpec spec (State s) = State s{tree=Tree.setSpec spec s.tree}

specInput :: State -> String
specInput (State s) = s.specInput

setSpecInput :: String -> State -> State
setSpecInput i (State s) = State s{specInput=i}

selectedPaths :: State -> List TreeTypes.Path
selectedPaths = Tree.selected <<< tree

selectedTreeObjs :: State -> Index.QueryResults
selectedTreeObjs s = Index.attrQuery q i
    where q = TreeTypes.inducedDisjunction (selectedPaths s)
          i = Project.index (proj s)
