-- |Simple types global to PMI apps.
--
-- A lot of newtype wrappers around strings because in general, at the UI
-- layer, we want users to be able to input any old thing like in Excel.
-- Later, we can warn them about empty values, nonsense values etc, but as an
-- editor, we should mostly not care so much about those things.  The newtype
-- wrappers allow us to provide Ord/Eq instances etc, and also makes the code
-- more readable and type-safe.
module Types
  ( UserId
  , userId
  , unUserId
  
  , Size
  , size
  , unSize

  , Area
  , area
  , unArea

  , Dwg
  , dwg
  , unDwg

  , Material
  , material
  , unMaterial

  , System
  , system
  , unSystem

  , WeldNumber
  , weldNumber
  , unWeldNumber

  , CustomAttrName
  , customAttrName
  , unCustomAttrName

  , CustomAttrValue
  , customAttrValue
  , unCustomAttrValue

  , User
  , user
  , username
  , password
  , Password
  , Username
  ) where


import Data.Eq ( class Eq )
import Data.Ord ( class Ord )
import Data.Show ( class Show
                 , show
                 )
import Id ( Id )
import Util ( NonEmptyTrimmedString(..) )


newtype UserId = UserId Id
userId :: Id -> UserId
userId = UserId
unUserId :: UserId -> Id
unUserId (UserId u) = u


-- |Centi-inches.
newtype Size = Size Int
derive instance sizeEq :: Eq Size
instance sizeShow :: Show Size where
  show (Size i) = show i -- TODO: Show pretty.
size :: Int -> Size
size = Size
unSize :: Size -> Int
unSize (Size i) = i


newtype Area = Area String
derive instance areaEq :: Eq Area
instance areaShow :: Show Area where
  show (Area s) = s
area :: String -> Area
area = Area
unArea :: Area -> String
unArea (Area s) = s


newtype Dwg = Dwg String
derive instance dwgEq :: Eq Dwg
instance dwgShow :: Show Dwg where
  show (Dwg s) = s
dwg :: String -> Dwg
dwg = Dwg
unDwg :: Dwg -> String
unDwg (Dwg s) = s


newtype Material = Material String
derive instance materialEq :: Eq Material
instance materialShow :: Show Material where
  show (Material s) = s
material :: String -> Material
material = Material
unMaterial :: Material -> String
unMaterial (Material s) = s


newtype System = System String
derive instance systemEq :: Eq System
instance systemShow :: Show System
  where show (System s) = s
system :: String -> System
system = System
unSystem :: System -> String
unSystem (System s) = s


-- |Common usage doesn't actually restrict value to a number.  Sometimes there
-- are letters in the value.
newtype WeldNumber = WeldNumber String
derive instance weldNumberEq :: Eq WeldNumber
instance weldNumberShow :: Show WeldNumber where
  show (WeldNumber s) = s
weldNumber :: String -> WeldNumber
weldNumber = WeldNumber
unWeldNumber :: WeldNumber -> String
unWeldNumber (WeldNumber s) = s


newtype CustomAttrName = CustomAttrName String
derive instance customAttrNameEq :: Eq CustomAttrName
derive instance customAttrNameOrd :: Ord CustomAttrName


customAttrName :: NonEmptyTrimmedString -> CustomAttrName
customAttrName (NonEmptyTrimmedString s) = CustomAttrName s

unCustomAttrName :: CustomAttrName -> String
unCustomAttrName (CustomAttrName s) = s


newtype CustomAttrValue = CustomAttrValue String
derive instance customAttrValueEq :: Eq CustomAttrValue
customAttrValue :: String -> CustomAttrValue
customAttrValue = CustomAttrValue
unCustomAttrValue :: CustomAttrValue -> String
unCustomAttrValue (CustomAttrValue s) = s


------------------------------------------------------------------------------
-- |A "logged in" user.  The Auth component only fills State with a User if
-- login was successful.
newtype User = User { username :: Username
                    , password :: Password
                    }

user :: Username -> Password -> User
user u p = User { username : u
                , password : p
                }

username :: User -> Username
username (User u) = u.username


password :: User -> Password
password (User u) = u.password



type Username = String
type Password = String
