module Event
    ( Event(..)
    , AuthFormEvent(..)
    , TreeEvent(..)
    ) where

import Data.Either ( Either )
import Data.Maybe ( Maybe )
import Error ( Error )
import Pmi.Lang.Types as Lang
import Pmi.Lang.V3.Db.Project as DbProj
import Project ( Project )
import Project as Project
import Stage ( Edit )
import Types ( User )
import UI.TreeTypes ( Path )


data Event = LoadProjectList
           | ProjectListLoaded (Either Error (Array DbProj.Listed))
           | LoadProject Lang.Id
           | ProjectLoaded Lang.Id (Either Error Project.Log)
           | AppendEdit Edit
           | Save Project
           | Saved Lang.Id (Maybe Error)
           | NewProject
           | Login User
           | Logout
           | AuthFormEvent AuthFormEvent
           | ShowAuthForm
           | CloseAuthForm
           | SetSpecInput String
           | TreeEvent TreeEvent


data AuthFormEvent = SetUsername String
                   | SetPassword String
                   | DoLogin
                   | LoginResponse (Either Error User)


data TreeEvent = ExpandPath Path
               | CollapsePath Path
               | AlsoSelectPath Path
               | OnlySelectPath Path
               | UnselectPath Path
