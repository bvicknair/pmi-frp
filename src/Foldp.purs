module Foldp ( foldp ) where

------------------------------------------------------------------------------
import Control.Applicative ( pure )
import Control.Monad.Eff.Class ( liftEff )
import Data.Either ( Either(Left, Right) )
import Data.Functor ( map )
import Data.List as List
import Data.Maybe ( Maybe(Just, Nothing) )
import Data.String as String
import Data.Tuple ( Tuple(..) )
import Effects ( AppEffects
               , noEffects
               )
import Event ( Event(..) )
import HTTP as HTTP
import Id ( Id( Local
              , Universal
              ))
import Index ( genUniversalIds )
import LocalId ( LocalId
               , genId
               , runLocalId
               )
import State ( State
             , appendEdit
             , authForm
             , clearErr
             , nextLocalId
             , notBusy
             , setAuthForm
             , setBusy
             , setErr
             , setNextLocalId
             , setProj
             , setProjList
             , setSpec
             , setSpecInput
             , setTree
             , setUser
             , tree
             , user
             )
import Pmi.Lang.Types as LangTypes
import Prelude ( bind
               , ($)
               , (<<<)
               , (+)
               )
import Project ( stagedExprs
               , project
               )
import Project as Proj
import Pux ( EffModel
           , mapState
           )
import Types ( Password
             , Username
             , password
             , username
             )
import UI.Auth as Auth
import UI.Tree as Tree
import UI.TreeTypes as TreeTypes
------------------------------------------------------------------------------




-- |A foldp that handles threading the next available locally unique id through
-- our actual foldp implementation.
--
-- Originally I thought we could just make all our foldp's a WriterT action,
-- but Pux.start requires a foldp that isn't monadic, so our workaround is to
-- have a top-level foldp that simply returns an EffModel record, but it runs
-- our real foldp, and any sub-module foldps, inside of a Monad so that we can
-- generate unique local ids seemlesly with a Writer monad.  This allows us to
-- write our foldp code in a clean way without having to explicitly pass around
-- the next available local id.
foldp :: Event -> State -> EffModel State Event AppEffects
foldp e s = let (Tuple result lastId) = runLocalId (_foldp e s) nextId
                newState = setNextLocalId (lastId+1) result.state
            in { state   : newState
               , effects : result.effects
               }
    where nextId = nextLocalId s


------------------------------------------------------------------------------
-- |Common scenario where we want to behave differently depending on if the
-- user is logged in currently or not.
ifLoggedIn :: State
           -> (Username -> Password -> LocalId (EffModel State Event AppEffects))
           -> LocalId (EffModel State Event AppEffects)
           -> LocalId (EffModel State Event AppEffects)
ifLoggedIn s then_ else_ = case user s of
                             Just u  -> then_ (username u) (password u)
                             Nothing -> else_


------------------------------------------------------------------------------
-- |The "real" foldp, that runs in our LocalId Monad.
_foldp :: Event -> State -> LocalId (EffModel State Event AppEffects)
_foldp (LoadProject id) s = ifLoggedIn s go noOp
    where noOp = noEffects s -- TODO: Warn user not logged in?
          go user pass = pure { state  : setBusy "Loading project" s
                              , effects: [load user pass]
                              }
          load u p = map (Just <<< ProjectLoaded id)
                         $ HTTP.getProject u p id

_foldp (ProjectLoaded _ (Left e)) s = noEffects $ setErr e $ notBusy s
_foldp (ProjectLoaded id (Right log)) s = noEffects
                                        $ setProj (project (Universal id) log)
                                        $ clearErr
                                        $ notBusy s
_foldp (AppendEdit e) s = noEffects $ appendEdit e s


_foldp (Save p) s = ifLoggedIn s go noOp
    where noOp = noEffects s -- TODO: Could update UI to warn them: "You can't
                             -- save, you're not logged in.", or can emit event
                             -- to open auth form first, or both etc.
          go username password = pure { state   : setBusy "Saving project" s
                                      , effects : [save username password]
                                      }
          -- |In the case of a new project, we have to assign a Universal Id to
          -- the project before we procede, because we'll need to know what the
          -- project's Id is to emit the Saved event.  The Saved event requires
          -- the project id so that we can possibly re-load the project on
          -- save.  Note that we don't actually replace the Project in the
          -- State, we simply attempt to save the new project with a Universal
          -- Id, and if it fails, the Saved handler will simply ignore the id
          -- we generated.  Thus, on the next save, we will generate a new id
          -- for the project and try to save again.  We keep trying to save
          -- with new generated ids each time until we are successful, in which
          -- case, the handler of the Saved event should re-load the project,
          -- resulting in an in-memory project structure with the Id we
          -- generated.  At first we simply used the genUniversalIds function
          -- to produce the expressions that needed to be saved, but that
          -- function didn't tell us (and can't really know) the new id it
          -- generated for a new project.  But we needed to know the id
          -- generated for the new project so that we could reload it on save
          -- success.  In the case of existing projects, we don't need to
          -- generate an id.
          save user pass = do id <- case Proj.id p of
                                         Universal id' -> pure id'
                                         Local _       -> liftEff LangTypes.genId
                                       -- NoOp for existing proj.
                              let p' = Proj.setId (Universal id) p
                              es <- liftEff $ genUniversalIds (stagedExprs p')
                              result <- HTTP.save user pass es
                              pure $ Just $ Saved id result

_foldp (Saved _ (Just e)) s = noEffects $ setErr e $ notBusy s

-- |Save was successful.  We reload the project from the server because:
-- 1) In the case of a new project, we want to replace the Project + Stage we
--    have in memory with a proper ProjectLog, which has Universal Ids assigned
--    to each object.  Instead of writing code to migrate our in-memory Stage
--    into in-memory ProjectLog, we just pull the data from the server both
--    because it is simpler, and because it gives the server a chance to
--    modify the ProjectLog if needed.  For example, maybe in the future the
--    server decides to add some default data objects to each new project that
--    is saved, and the only way we the client would know about them is to load
--    the project from the server once we've saved a new project.
--  2) In the case that we saved an existing project, we want to re-load the
--     project now to pull in any changes other clients may have made.
--
-- In general, I think the "re-load project on save" method is simple an
-- effective, at the expense of network bandwidth.  If bandwidth becomes a
-- bottleneck, I can change this behavior.
_foldp (Saved id Nothing) s = pure { state  : clearErr $ notBusy s
                                   , effects: [reload]
                                   }
    where reload = pure $ Just $ LoadProject id

_foldp LoadProjectList s = ifLoggedIn s go noOp
    where noOp = noEffects s -- TODO: Could update UI to warn them: "You can't
                             -- load projects, you're not logged in.", or can
                             -- emit event to open auth form first, or both
                             -- etc.
          go username password = pure { state   : setBusy "Loading projects" s
                                      , effects : [load username password]
                                      }
          load u p = map (Just <<< ProjectListLoaded)
                         $ HTTP.getProjectList u p
_foldp (ProjectListLoaded (Left e)) s = noEffects $ setErr e $ notBusy s
_foldp (ProjectListLoaded (Right ps)) s = noEffects
                                        $ setProjList ps
                                        $ clearErr
                                        $ notBusy s
_foldp NewProject s = do id <- genId
                         noEffects $ setProj (project id emptyLog) s
  where emptyLog = []


-- TODO: Handle opening/closing of auth form.  Auth form can also emit
-- CloseAuthForm after successful login or on cancel.
_foldp (Login u) s = noEffects $ setUser (Just u) s
_foldp Logout s = noEffects $ setUser Nothing s

_foldp (AuthFormEvent e) s =
  case authForm s of
       Nothing -> noEffects s
       Just a  -> do result <- Auth.foldp e a
                     pure $ mapState updateAuth result
    where updateAuth auth = setAuthForm (Just auth) s

_foldp (TreeEvent e) s = do result <- Tree.foldp e (tree s)
                            pure $ mapState updateTree result
  where updateTree treeState = setTree treeState s

_foldp ShowAuthForm s = case authForm s of
                          Just _  -> alreadyOpen
                          Nothing -> openIt
    where authState = Just $ Auth.init (user s)
          alreadyOpen = noEffects s
          openIt      = noEffects $ setAuthForm authState s
_foldp CloseAuthForm s = noEffects $ setAuthForm Nothing s

_foldp (SetSpecInput i) s = noEffects $ setSpecInput i
                                      $ setSpec (parseSpecInput i) s


parseSpecInput :: String -> TreeTypes.Spec
parseSpecInput = List.fromFoldable <<< map String.trim <<< String.split delim
  where delim = String.Pattern ","
