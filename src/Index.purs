-- |A view of the current state of a project via folding over the project log
-- and the staging area.
--
-- This module provides a data structure that folds over a project log (+ any
-- changes in staging) and can provides efficiently read-only querying of the
-- "current state" of a project.
module Index
    ( Index

      -- The expr Index consumes to mutate state.
    , Expr(..)

      -- Lift Pmi.Lang.V3.Expr to Index.Expr.
    , liftExpr

      -- Index.Expr -> V3.Expr w/ effectful id creation via uuids.
    , genUniversalIds

      -- Index creation.
    , empty

      -- Index generation.
    , steprCommit
    , steplCommit
    , foldrLog
    , foldlLog
    , foldrUncommitted
    , foldlUncommitted

    , projectName
    
    -- Querying
    , Query(..)
    , QueryResult
    , QueryResults
    , AttrPredicate(..)
    , AttrQuery
    , AttrName
    , AttrValue
    , Type(..)
    , attrQuery
    , selAttr
    ) where

------------------------------------------------------------------------------
import Control.Applicative ( pure )
import Control.Monad.Eff ( Eff )
import Data.Array ( snoc )
import Data.Foldable ( class Foldable
                     , foldl
                     , foldM
                     , foldr
                     )
import Data.Functor ( map )
import Data.List ( catMaybes )
import Data.Map ( Map )
import Data.Map as Map
import Data.Maybe ( Maybe(Just, Nothing)
                  , maybe
                  )
import Data.Set ( Set )
import Data.Set as Set
import Data.Tuple ( Tuple(..) )
import Data.UUID ( GENUUID )
import Id ( Id(Local, Universal) )
import Pmi.Lang.Types ( CommitMsg
                      , ProjectName -- TODO: This or Types.ProjectName?
                      , Tag
                      , Timestamp
                      , UserId
                      , Username
                      , WeldMaterial
                      , WeldSize
                      )
import Pmi.Lang.Types as LangTypes
import Pmi.Lang.V3 ( Commit(..) )
import Pmi.Lang.V3 as Lang
import Prelude ( class Eq
               , class Ord
               , class Show
               , bind
               , show
               , ($)
               )
import Prelude as P
------------------------------------------------------------------------------


-- |State of a fold over a project log.  Provides efficient read-only querying
-- of the "current state" of a project.
newtype Index = Index { projName :: Map Id ProjectName 

                        -- |Current attribute values as strings for all
                        -- objects.
                      , curAttr :: Map (Tuple Type Id) (Map AttrName AttrValue)

                        -- |Index for looking up objects whose value for some
                        -- attribute is equal to some value.
                      , eqAttr :: Map (Tuple AttrName AttrValue) (Set QueryResult)
                      }


empty :: Index
empty = Index { projName : Map.empty
              , eqAttr   : Map.empty
              , curAttr  : Map.empty
              }


-- |The type of expressions an Index understands.  A copy of Pmi.Lang.V3.Expr,
-- but Id is the more general type (Universal or Local).
data Expr = NoOp -- Some Lang.Expr don't mutate an Index, like UserHasPasswordHash
          | UserHasName Id Username
          | ProjectNamed Id ProjectName
          | PipeWeldOnProject Id Id
          | PipeWeldHasSize Id WeldSize
          | PipeWeldHasMaterial Id WeldMaterial

liftExpr :: Lang.Expr -> Expr
liftExpr (Lang.UserHasName i n) = UserHasName (Universal i) n
liftExpr (Lang.UserHasPasswordHash _ _) = NoOp
liftExpr (Lang.ProjectNamed i n) = ProjectNamed (Universal i) n
liftExpr (Lang.PipeWeldOnProject i1 i2) = PipeWeldOnProject (Universal i1) (Universal i2)
liftExpr (Lang.PipeWeldHasSize i s) = PipeWeldHasSize (Universal i) s
liftExpr (Lang.PipeWeldHasMaterial i m) = PipeWeldHasMaterial (Universal i) m



-- |Generate Lang.Expr from Expr, generating unique ids for any Local ids.
--
-- Exprs with the same Local id value will be assigned the same unique
-- universal id.  For example, if two Expr hold (Local 5), (Local 5) will
-- result in the generation of some unique (Universal id), and the local id of
-- the original Expr will be replaced with this universal id when converted to
-- a Lang.Expr.  In this way, this function processes a batch of Expr, assuring
-- that references remain valid, only changing Local to Universal.  It is not
-- guaranteed that multiple calls to this function will generate the same
-- Universal id for a given Local id however, so in practice, this function
-- should be used just before saving staged edits to the server so unique ids
-- are assigned to objects before saving.  If the save fails, and this function
-- is called again after the user has made some changes, different universal
-- ids will be generated for the same objects as in the previous save.  Once
-- the exprs have successfully been saved, the server will "know" about the ids
-- we've generated, so it would be bad if on the client side we still held the
-- Expr with local ids that could potentially be assigned new universal ids if
-- this function is called again, so we should probably dump any Expr we have
-- in memory on save an reload from the server on successful save.
genUniversalIds :: forall e f. Foldable f
                => f Expr
                -> Eff (uuid :: GENUUID |e ) (Array Lang.Expr)
genUniversalIds es = do state <- foldM idStep init es
                        pure $ state.results
    where init = { ids     : Map.empty
                 , results : []
                 }

-- |Step the 'genUniversalIds' fold forward.
idStep :: forall e. GenIdState -> Expr -> Eff (uuid :: GENUUID |e ) GenIdState
idStep s NoOp = pure s
idStep s (UserHasName _ _) = pure s -- read-only expr in this app.
idStep s (ProjectNamed id n) = do (Tuple ids' uid) <- genId s.ids id
                                  let e = Lang.ProjectNamed uid n
                                  pure s{ ids     = ids'
                                        , results = snoc s.results e
                                        }
idStep s (PipeWeldOnProject id1 id2) =
    do (Tuple ids' uid1)  <- genId s.ids id1
       (Tuple ids'' uid2) <- genId ids' id2
       let e = Lang.PipeWeldOnProject uid1 uid2
       pure s{ ids     = ids''
             , results = snoc s.results e
             }
idStep s (PipeWeldHasSize id a) = do (Tuple ids' uid) <- genId s.ids id
                                     let e = Lang.PipeWeldHasSize uid a
                                     pure s{ ids     = ids'
                                           , results = snoc s.results e
                                           }
idStep s (PipeWeldHasMaterial id a) = do (Tuple ids' uid) <- genId s.ids id
                                         let e = Lang.PipeWeldHasMaterial uid a
                                         pure s{ ids     = ids'
                                               , results = snoc s.results e
                                               }


-- |Generates id if needed, and if it does, updates the id map.
genId :: forall e.
         Map LocalId LangTypes.Id
      -> Id
      -> Eff (uuid :: GENUUID |e ) (Tuple (Map LocalId LangTypes.Id) LangTypes.Id)
genId m (Universal id) = pure $ Tuple m id
genId m (Local id) = case Map.lookup id m of
                       Just uid -> pure (Tuple m uid)
                       Nothing  -> do uid <- LangTypes.genId
                                      let m'= Map.insert id uid m
                                      pure $ Tuple m' uid


-- |Local ids are just ints.
type LocalId = Int

-- |The state of the fold in 'genUniversalIds' that generates ids for all local
-- ids.  A mapping of local id's for which a universal id has been generated to
-- that universal id, and the expressions that have been generated so far.
type GenIdState = { ids     :: Map LocalId LangTypes.Id
                  , results :: Array Lang.Expr
                  }






-- ===========================================================================
-- Stepping the Index forward
-- ===========================================================================


-- |Step an Index forward by one Expr.
--
-- Not every expr has been committed, hence Maybe CommitMeta.
step :: Maybe CommitMeta -> Expr -> Index -> Index
step _ (ProjectNamed id name) i = projectNamed id name i
step _ NoOp i = i
step _ (UserHasName _ _) i = i
step _ (PipeWeldOnProject weldId projId) i =
    setCurAttr Weld weldId "onProject" (show projId) i
step _ (PipeWeldHasSize id size) i = setCurAttr Weld id "size" (show size) i
step _ (PipeWeldHasMaterial id mat) i =
    setCurAttr Weld id "material" (show mat) i


-- |Fold over expressions of a commit from the right.
steprCommit :: Commit -> Index -> Index
steprCommit cm@(Commit c) i = foldr (step meta) i (map liftExpr c.commitExprs)
    where meta = Just $ toMeta cm


-- |Fold over expressions of a commit from the left.
steplCommit :: Index -> Commit -> Index
steplCommit i cm@(Commit c) = foldl step' i (map liftExpr c.commitExprs)
    where meta = Just $ toMeta cm
          step' index expr = step meta expr index


-- |Fold over a sequence of commits from the right, folding over the
-- expressions in each commit from the right.
foldrLog :: forall f. Foldable f => Index -> f Commit -> Index
foldrLog = foldr steprCommit


-- |Fold over a sequence of commits from the left, folding over the
-- expressions in each commit from the left.
foldlLog :: forall f. Foldable f => Index -> f Commit -> Index
foldlLog = foldl steplCommit


foldrUncommitted :: forall f. Foldable f => Index -> f Expr -> Index
foldrUncommitted = foldr (step Nothing)

foldlUncommitted :: forall f. Foldable f => Index -> f Expr -> Index
foldlUncommitted = foldl step'
    where step' index expr = step Nothing expr index



projectNamed :: Id -> ProjectName -> Index -> Index
projectNamed id name (Index i) = Index i{projName=new}
    where new = Map.insert id name i.projName



-- ===========================================================================
-- Maintaining internal indexes
-- ===========================================================================

------------------------------------------------------------------------------
curAttr :: Index -> Map (Tuple Type Id) (Map AttrName AttrValue)
curAttr (Index i) = i.curAttr

------------------------------------------------------------------------------
setCurAttr :: Type -> Id -> AttrName -> AttrValue -> Index -> Index
setCurAttr typ id n v i@(Index irec) = 
  Index irec{ curAttr = newMap
            , eqAttr  = updateEqAttr typ id n curVal v (eqAttr i)
            }
    where curObj = maybe emptyObj P.id $ Map.lookup (Tuple typ id) (curAttr i)
          curVal = maybe "" P.id $ Map.lookup n curObj
          newObj = Map.insert n v curObj
          newMap = Map.insert (Tuple typ id) newObj (curAttr i)
          emptyObj = Map.empty


------------------------------------------------------------------------------
-- |We must take care to remove the old value from the eqAttr index as well as
-- insert the new one.
updateEqAttr :: Type
             -> Id
             -> AttrName
             -> AttrValue
             -> AttrValue
             -> Map (Tuple AttrName AttrValue) (Set QueryResult)
             -> Map (Tuple AttrName AttrValue) (Set QueryResult)
updateEqAttr typ id n oldVal newVal map = next
    where prevOld = maybe Set.empty P.id $ Map.lookup (Tuple n oldVal) map
          nextOld = Set.delete qr prevOld
          prevNew = maybe Set.empty P.id $ Map.lookup (Tuple n newVal) map
          nextNew = Set.insert qr prevNew
          next    = Map.insert (Tuple n newVal) nextNew
                  $ Map.insert (Tuple n oldVal) nextOld map
          qr      = Tuple typ id





------------------------------------------------------------------------------
getCurAttr :: Type -> Id -> AttrName -> Index -> Maybe AttrValue
getCurAttr typ id n i = case maybeObj of
                          Nothing  -> Nothing
                          Just obj -> Map.lookup n obj
    where maybeObj = Map.lookup (Tuple typ id) (curAttr i)



------------------------------------------------------------------------------
eqAttr :: Index -> Map (Tuple AttrName AttrValue) (Set QueryResult)
eqAttr (Index i) = i.eqAttr


------------------------------------------------------------------------------
setEqAttr :: Map (Tuple AttrName AttrValue) (Set QueryResult)
          -> Index
          -> Index
setEqAttr v (Index i) = Index i{eqAttr=v}


------------------------------------------------------------------------------
attrEqual :: AttrName -> AttrValue -> Index -> QueryResults
attrEqual n v i = emptySetOnNothing $ Map.lookup (Tuple n v) (eqAttr  i)


------------------------------------------------------------------------------
-- |Utility function for common case of turning missing value in a map to an
-- empty set of query results.
emptySetOnNothing :: forall a. Maybe (Set a) -> Set a
emptySetOnNothing Nothing = Set.empty
emptySetOnNothing (Just s) = s





------------------------------------------------------------------------------
-- |To simplify passing around *only* the meta of a commit, without all the
-- expressions.
newtype CommitMeta = CommitMeta { commitTs    :: Timestamp
                                , commitUser  :: UserId
                                , commitMsg   :: CommitMsg
                                , commitTags  :: Array Tag
                                }


toMeta :: Commit -> CommitMeta
toMeta (Commit c) = CommitMeta { commitTs    : c.commitTs
                               , commitUser  : c.commitUser
                               , commitMsg   : c.commitMsg
                               , commitTags  : c.commitTags
                               }


-- ===========================================================================
-- Read API Functions
-- ===========================================================================

------------------------------------------------------------------------------
projectName :: Id -> Index -> Maybe ProjectName
projectName id (Index i) = Map.lookup id i.projName




-- ===========================================================================
-- Queries
-- ===========================================================================

------------------------------------------------------------------------------
-- |A Query language for some type of predicate 'p'.
data Query p = All -- Matches any object.
             | None -- Matches no object.
             | IsTrue  p
             | And   (Query p) (Query p)
             | Or    (Query p) (Query p)
             | Not   (Query p)

------------------------------------------------------------------------------
-- |For a query interface where all objects are viewed as a collection of
-- AttrName/AttrValue pairs, where the values are all strings.
--
-- At the time of this writing, I'm not sure if a "typed" or "untyped" (all
-- strings) query language will be more useful, or if both will be useful.  I'm
-- starting with a simple string-based query language, that may incorporate
-- some casting etc.  The alternative is something like:
--
--  -- |A constructor for every possible predicate.
--  data ObjectPredicate = MaterialEquals Material
--                       | SizeEqual Size
--
-- But for now, we'll just do a string-based interface.
data AttrPredicate = AttrEqual AttrName AttrValue
type AttrQuery = Query AttrPredicate
type AttrName = String
type AttrValue = String

------------------------------------------------------------------------------
-- |If an object matches a query, then there will be a (type,id) tuple in the
-- result.
type QueryResult = Tuple Type Id
type QueryResults = Set QueryResult

------------------------------------------------------------------------------
-- |Every type of queryable of object supported by this module.
data Type = Weld
derive instance ordType :: Ord Type
derive instance eqType :: Eq Type
instance showType :: Show Type where
    show Weld = "Weld"




------------------------------------------------------------------------------
-- |Given a Query of Predicate type 'p', an Index, and a function that can
-- answer Predicates of type 'p', get QueryResults.
query :: forall p. 
         (p -> Index -> QueryResults)
      -> Query p
      -> Index
      -> QueryResults
query _ All i = allObjs i
query _ None _ = Set.empty
query f (IsTrue p) i = f p i
query f (And q1 q2) i = Set.intersection (query f q1 i) (query f q2 i)
query f (Or q1 q2) i = Set.union (query f q1 i) (query f q2 i)
query f (Not q) i = Set.difference (allObjs i) (query f q i)

------------------------------------------------------------------------------
attrQuery :: AttrQuery -> Index -> QueryResults
attrQuery = query attrPred


------------------------------------------------------------------------------
-- |Select value of a single attribute from all objects in a result.
selAttr :: AttrName -> QueryResults -> Index -> Set AttrName
selAttr name results i = Set.fromFoldable $ catMaybes $ map get asList
    where get res = do obj <- Map.lookup res (curAttr i)
                       Map.lookup name obj
          asList = (Set.toUnfoldable results)


------------------------------------------------------------------------------
-- |A QueryResult for every object for which the predicate is true.
attrPred :: AttrPredicate -> Index -> QueryResults
attrPred (AttrEqual n v) i = attrEqual n v i


------------------------------------------------------------------------------
-- |Every object as a QueryResult.
allObjs :: Index -> QueryResults
allObjs i = Set.fromFoldable $ Map.keys (curAttr i)
