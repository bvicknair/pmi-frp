module UI
    ( view
    ) where


-- ===========================================================================
-- Design Ideas
-- ===========================================================================
-- A user experience that is delightful and maybe even fun to use like a Sid
-- Meier game UI.  Fast, dynamic, flexible, useful, colorful.
--
-- When looking at group of objects at some node in the tree, color code the
-- objects by material.  "Primary" material (the most items in the group have
-- that material) is one color, other materials are assigned a different color.
-- Each material doesn't have a color, but rather, primary, secondary, third
-- most items of that material etc.
-- Maybe there are several view options to show gruoped/colored by material,
-- bore, completed status etc.
--
-- Maybe all objects in UI layer are simply a (Map String -> String), then
-- special handle some attributes as being a certain type etc.  Maybe each
-- object also has a required "type" so an object becomes:
--   (Type, Map String String)
--
-- This may make querying, showing many different types of objects in the UI
-- easier.  Then there is some layer that converts the bag of strings to Expr
-- to make changes.  Any object can have any attribute attached to it.  The
-- query language would need to implicitly/explicitly cast from string to
-- number/date etc to provide some filtering/sorting cababilities.



-- !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
-- TODO: Don't enable certain buttons when user is not logged in, or don't even
-- draw them.  Have to guide user with clues.
-- !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!


------------------------------------------------------------------------------
import Data.Foldable ( for_ )
import Data.Foreign.NullOrUndefined ( unNullOrUndefined )
import Data.Maybe ( Maybe(Just, Nothing)
                  , fromJust
                  , maybe
                  )
import Data.Monoid ( (<>) )
import Data.Set as Set
import Error ( prettyError )
import Event ( Event( AppendEdit
                    , LoadProject
                    , LoadProjectList
                    , Logout
                    , NewProject
                    , Save
                    , SetSpecInput
                    , ShowAuthForm
                    ))
import Partial.Unsafe ( unsafePartial ) -- TODO: Remove after debug
import Pmi.Lang.Types as Lang
import Pmi.Lang.V3.Db.Project as DbProj
import Prelude ( const
               , discard
               , show
               , ($)
               , (<<<)
               )
import Project as Project
import Pux.DOM.Events ( onChange
                      , onClick
                      , targetValue
                      )
import Pux.DOM.HTML ( HTML )
import Stage ( Edit(SetProjectName) )
import State ( State(..) )
import State as State
import Text.Smolder.HTML as H
import Text.Smolder.HTML.Attributes as A
import Text.Smolder.Markup ( text
                           , (!)
                           , (#!)
                           )
import Types ( User
             , username
             )
import UI.Auth as Auth
import UI.Tree as Tree
------------------------------------------------------------------------------


------------------------------------------------------------------------------
view :: State -> HTML Event
view s = case State.user s of
           Nothing -> notLoggedIn s
           Just u  -> loggedIn u s


------------------------------------------------------------------------------
notLoggedIn :: State -> HTML Event
notLoggedIn s = case State.authForm s of
                     Nothing  -> H.button #! onClick (const ShowAuthForm)
                                          $ text "Login"
                     Just auth -> Auth.view auth


------------------------------------------------------------------------------
loggedIn :: User -> State -> HTML Event
loggedIn u s = H.div do
  H.h2 (text $ projectName s)
  H.div do
    case State.authForm s of
         Nothing  -> H.button #! onClick (const Logout)
                              $ text $ "Logout as " <> username u
         Just auth -> Auth.view auth
  H.div ! A.className "projectMenu" $ do
    text "Project menu: "
    H.button #! onClick loadProjs $ text "Load project list"
    projList (State.projList s)
    H.button #! onClick (const NewProject) $ text "New Project"
    H.button #! onClick rename  $ text "Rename project"
    H.button #! onClick (save $ State.proj s)$ text "Save"
  case State.busy s of
    Nothing -> text ""
    Just m  -> text m
  case State.err s of
    Nothing -> text ""
    Just e  -> text $ "Error: " <> (prettyError e)
  -- H.pre (text $ show $ State.proj s)
  H.input ! A.type' "text"
          ! A.value (State.specInput s)
         #! onChange (SetSpecInput <<< targetValue)
  Tree.view (State.proj s) (State.tree s)
  editor s
  where loadProjs = const $ LoadProjectList
        rename = const $ AppendEdit $ SetProjectName (debugProjectName "foo")
        save p = const $ Save p


------------------------------------------------------------------------------
projectName :: State -> String
projectName (State s) = maybe "<Unnamed project>" show $ Project.name s.proj


------------------------------------------------------------------------------
debugProjectName :: String -> Lang.ProjectName
debugProjectName s = unsafePartial $ fromJust $ Lang.lineFromString s


------------------------------------------------------------------------------
projList :: Array DbProj.Listed -> HTML Event
projList ps = H.ul (for_ ps item)
  where item (DbProj.Listed p) = H.li $ H.a ! A.href "#"
                                            #! onClick (load p.listedId)
                                            $ (text (name p))
        load = const <<< LoadProject
        name p = case unNullOrUndefined p.listedName of
                      Nothing -> "<unnamed>"
                      Just n  -> show n


------------------------------------------------------------------------------
-- |Debug view to show what is selected in tree.
editor :: State -> HTML Event
editor s = H.div $ do text $ (show (Set.size sel)) <> " matching objects: "
                      text (show sel)
    where sel = State.selectedTreeObjs s
