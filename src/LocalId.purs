-- |A Monad that generates unique Local Ids.
--
-- This Monad provides a pure (no purescript effects) way to generate unique
-- ids by simply incrementing an integer.
--
-- Local Ids are used by objects that have not been saved to the server yet.
-- Once saved to the server, objects are assigned a UUID.
module LocalId
  ( LocalId
  , runLocalId
  , genId
  ) where

import Control.Applicative ( class Applicative
                           , pure
                           )
import Control.Apply ( class Apply )
import Control.Bind ( class Bind )
import Control.Monad ( class Monad )
import Control.Monad.State.Trans ( StateT
                                 , get
                                 , modify
                                 , runStateT
                                 )
import Data.Identity ( Identity(..) )
import Data.Functor ( class Functor
                    , map
                    )
import Data.Tuple ( Tuple )
import Id ( Id(Local) )
import Prelude ( bind
               , discard
               , (+)
               )


newtype LocalId a = LocalId (StateT Int Identity a)
derive newtype instance functorLocalId :: Functor LocalId
derive newtype instance applyLocalId :: Apply LocalId
derive newtype instance applicativeLocalId :: Applicative LocalId
derive newtype instance bindLocalId :: Bind LocalId
derive newtype instance monadLocalId :: Monad LocalId


runLocalId :: forall a. LocalId a -> Int -> Tuple a Int
runLocalId (LocalId m) i = case runStateT m i of
                                  Identity t -> t

genId :: LocalId Id
genId = LocalId (map Local genInt)

genInt :: StateT Int Identity Int
genInt = do current <- get
            modify increment
            pure current
  where increment s = s + 1

