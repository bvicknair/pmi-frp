-- |Values to uniquely identify objects in a conceptual memory space.
module Id
  ( Unique(..)
  , Id(..)
  ) where

------------------------------------------------------------------------------
import Data.Eq ( class Eq
               , eq
               )
import Data.Monoid ( (<>) )
import Data.Ord ( class Ord
                , Ordering(GT, LT)
                , compare
                )
import Data.Show ( class Show
                 , show
                 )
import Pmi.Lang.Types as Lang
------------------------------------------------------------------------------



-- |A unique value, identifiable by an Id.
data Unique a = Unique Id a


-- |An Id is either universally unique, or only locally unique.
--
-- Universal Ids are for objects that have been saved to the server.
-- Local Ids are for objects that are in the client's buffer and haven't
-- been saved to the conceptual PMI memory space yet.
data Id = Universal Lang.Id
        | Local     Int

-- |We arbitrarily choose Universal to be > Local.
instance ordId :: Ord Id where
  compare (Universal a) (Universal b) = compare a b
  compare (Local a)     (Local b)     = compare a b
  compare (Universal a) (Local b)     = GT
  compare (Local a)     (Universal b) = LT

instance showId :: Show Id where
    show (Universal id) = "Universal#" <> Lang.idToString id
    show (Local id)     = "Local#" <> show id


instance idEq :: Eq Id where
  eq (Universal a) (Universal b) = eq a b
  eq (Local a) (Local b)         = eq a b
  eq _         _                 = false
