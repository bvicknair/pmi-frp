module Util
  ( PositiveInt(..)
  , positiveInt
  , unPositiveInt

  , TrimmedString(..)
  , trimmedString
  , unTrimmedString

  , NonEmptyTrimmedString(..)
  , nonEmptyTrimmedString
  ) where


------------------------------------------------------------------------------
import Data.Int as Int
import Data.Maybe ( Maybe(Just, Nothing) )
import Data.String as String
import Prelude ( bind
               , ($)
               , (<)
               )
------------------------------------------------------------------------------

newtype PositiveInt = PositiveInt Int

unPositiveInt :: PositiveInt -> Int
unPositiveInt (PositiveInt i) = i

positiveInt :: String -> Maybe PositiveInt
positiveInt s = do i <- Int.fromString s
                   if i < 1
                     then Nothing
                     else Just $ PositiveInt i



newtype TrimmedString = TrimmedString String

unTrimmedString :: TrimmedString -> String
unTrimmedString (TrimmedString s) = s

trimmedString :: String -> TrimmedString
trimmedString s = TrimmedString $ String.trim s

newtype NonEmptyTrimmedString = NonEmptyTrimmedString String

-- |Nothing if trimmed string is empty.
nonEmptyTrimmedString :: String -> Maybe NonEmptyTrimmedString
nonEmptyTrimmedString s = if String.null trimmed
                          then Nothing
                          else Just $ NonEmptyTrimmedString trimmed
  where trimmed = String.trim s
