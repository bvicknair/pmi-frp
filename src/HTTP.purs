-- |HTTP communication with API server.
--
-- NOTE: using 'attempt' w/ affjax library returns a Left result when there
-- is an HTTP transport problem: no connection, broken socket, malformed URL
-- etc.  It returns Right AffjaxResponse in all other cases, and we must check
-- the status code and response for server-specific errors.
--
-- This module shouldn't import Event, it shouldn't know about Events.  Foldp
-- is the glue that binds HTTP actions together with our Event model.
module HTTP
    ( getProject
    , getProjectList
    , login
    , save
    ) where


import Control.Applicative ( pure )
import Control.Monad.Aff ( Aff
                         , attempt
                         )
import Control.Monad.Except ( runExcept )
import Data.Either ( Either(Left, Right) )
import Data.Foreign ( Foreign )
import Data.Foreign.Class ( class Decode
                          , decode
                          )
import Data.Foreign.Generic ( encodeJSON )
import Data.Foreign.JSON ( parseJSON )
import Data.HTTP.Method ( Method( GET
                                , PUT
                                ) )
import Data.Maybe ( Maybe(Just, Nothing) )
import Data.MediaType ( MediaType(..) )
import Data.Monoid ( (<>) )
import Error ( Error( AjaxError
                    , BadLogin
                    , BadJSON
                    , UnexpectedStatusCode
                    )
              )
import Error as Error
import Network.HTTP.Affjax ( AJAX
                           , AffjaxRequest
                           )
import Network.HTTP.Affjax as Affjax
import Network.HTTP.RequestHeader ( RequestHeader( Accept
                                                 , ContentType
                                                 ))
import Network.HTTP.StatusCode ( StatusCode(..) )
import Pmi.Lang.Types ( Id
                      , idToString
                      ) as Lang
import Pmi.Lang.V3 ( Expr ) as Lang
import Pmi.Lang.V3.Db.Project as DbProj
import Prelude ( Unit
               , bind
               , show
               , ($)
               )
import Project as Project
import Types ( User 
             , user 
             )

-- TODO: Should all HTTP communication be moved to pmi-lang-ps or
-- pmi-lang-ps-http?  Then we could just use the API of that library which
-- could hide all the details about error parsing etc.  We would need a way to
-- pass in URLs though, or some sort of config for generating them.  This would
-- make it easier to build other clients besides this one; admin clients,
-- specialty clients etc.  Anything that isn't State/Foldp specific could be
-- put into such a library for clients to use.  Maybe pmi-lang-ps becomes that
-- library?  We'd be mixing HTTP into pmi-lang which theoretically doesn't have
-- anything to do with HTTP though, so maybe a separate package
-- purescript-pmi-lang-http. And maybe rename pmi-lang-ps to
-- purescript-pmi-lang.

------------------------------------------------------------------------------
-- |A version of Affjax 'get' that interprets the server response as either a
-- decodable type on status 200, or an error in all other cases.  This converts
-- the general HTTP error codes etc into our apps error type.
--
-- My first implementation had Affjax.get w/ a type of (AffjaxResponse
-- Foreign), but I found out that this would fail on empty response bodies.
-- The Affjax library tries to coherce the body into Foreign even if the body
-- is empty, resulting in an error even before I can check the status code.
-- Therefore, now I ask Affjax.get for a string response body, and I parse it
-- as JSON in paresResponse *if* a body is expected.  For example, for 403
-- responses, a body isn't expected so I don't even try to parse it as JSON.
get :: forall a e. Decode a => Username
                            -> Password
                            -> URL
                            -> Aff ( ajax::AJAX | e) (Either Error a)
get u p url = do
    tryRes <- attempt $ Affjax.affjax req
    case tryRes of
      Left e    -> pure $ Left (AjaxError e)
      Right res -> pure $ parse res.status res.response
  where req :: AffjaxRequest Unit
        req = { method          : Left GET
              , url             : url
              , headers         : [Accept $ MediaType "application/json"]
              , content         : Nothing
              , username        : Just u
              , password        : Just p
              , withCredentials : true
              }


type Username = String
type Password = String
type URL = String


------------------------------------------------------------------------------
getProject :: forall e.
              Username
           -> Password
           -> Lang.Id
           -> Aff (ajax :: AJAX | e) (Either Error Project.Log)
getProject u p id = get u p url
  where url = "http://127.0.0.1/api/projectlog/" <> Lang.idToString id


------------------------------------------------------------------------------
getProjectList :: forall e.
                  Username
               -> Password
               -> Aff (ajax :: AJAX | e) (Either Error (Array DbProj.Listed))
getProjectList u p = get u p url
  where url = "http://127.0.0.1/api/projects/"


------------------------------------------------------------------------------
save :: forall e.
        Username
     -> Password
     -> Array Lang.Expr
     -> Aff (ajax :: AJAX | e) (Maybe Error)
save u p es = do
    tryRes <- attempt $ Affjax.affjax req
    case tryRes of
      Left e    -> pure $ Just (AjaxError e)
      Right res -> pure $ check res.status res.response
    where json = encodeJSON es
          req :: AffjaxRequest String
          req  = { method          : Left PUT
                 , url             : url
                 , headers         : [ Accept      $ MediaType "application/json"
                                     , ContentType $ MediaType "application/json"
                                     ]
                 , content         : Just json
                 , username        : Just u
                 , password        : Just p
                 , withCredentials : true
                 }
          url = "http://127.0.0.1/api/commit"


------------------------------------------------------------------------------
-- |Nothing on successful login, Just Error otherwise.
login :: forall e.
         Username
      -> Password
      -> Aff (ajax :: AJAX | e) (Either Error User)
login u p = do
    tryRes <- attempt $ Affjax.affjax req
    case tryRes of
        Left e    -> pure $ Left $ AjaxError e
        Right res -> pure $ case check res.status res.response of
                              Nothing -> Right $ user u p
                              Just e  -> Left e
  where req :: AffjaxRequest Unit
        req  = { method          : Left GET
               , url             : url
               , headers         : [ Accept      $ MediaType "application/json"
                                   , ContentType $ MediaType "application/json"
                                   ]
               , content         : Nothing
               , username        : Just u
               , password        : Just p
               , withCredentials : true
               }
        -- |Any private api endpoint would work, selection is arbitrary because
        -- we don't actually read data from the endpoint response body, just
        -- check the HTTP status codes for successful Basic Auth login or not.
        url = "http://127.0.0.1/api/whoAmI"



------------------------------------------------------------------------------
-- |Interpret response from server as either an Error, or some decodable type.
parse :: forall a. Decode a => StatusCode -> ResponseBody -> Either Error a
parse s r = case check s r of
                 Just e  -> Left e
                 -- Must be status 200 then.
                 Nothing -> do f <- jsonStringToForeign r
                               case runExcept $ decode f of
                                 Left e  -> let bad = r
                                                err = show e
                                            in Left $ BadJSON err bad
                                 Right a -> Right a


------------------------------------------------------------------------------
-- |Check a reponse for an error.
--
-- Only Nothing on status code 200.
--
-- Expected responses from server
-- 403: In case of invalid login credentials.  There will be no response body,
--      so we won't try to convert the body to a Foreign at all.  Affjax
--      automatically was doing this in my first implementation which is why we
--      now avoid asking Affjax to give us a Foreign reponse body and just ask
--      for it as a string.
-- 200: Body is either empty, or is a JSON string that represents some
--      Decode'able type.  This function doesn't inspect the response body on
--      200 because sometimes the body will be empty so we don't want to
--      attempt to parse it as JSON or we'll get an error.
-- 400: Body represents an app-specific error encoded in JSON.  HTTP error
--      codes are not fine-grained enough to communicate all possible app
--      errors, so all errors get the 400 status code, and then the body will
--      provide more details.

check :: StatusCode -> ResponseBody -> Maybe Error
check (StatusCode 403) _ = Just BadLogin
check (StatusCode 200) _ = Nothing
check (StatusCode 400) s =
  case jsonStringToForeign s of
       Left e  -> Just e
       Right f -> case runExcept $ decode f of
                       Left e -> let bad = s
                                     err = show e
                                 in Just $ BadJSON err bad
                       Right jsonErr -> Just $ Error.liftJSON jsonErr
check s m = Just $ UnexpectedStatusCode s m


------------------------------------------------------------------------------
-- |Interpret a JSON string as a foreign.
--
-- Simply lifts parseJSON's Left error type into our Error type.
jsonStringToForeign :: String -> Either Error Foreign
jsonStringToForeign s = case runExcept $ parseJSON s of
                          Left e  -> Left $ BadJSON (show e) s
                          Right f -> Right f


-- |For clearer type signatures.
type ResponseBody = String
