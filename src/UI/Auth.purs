-- |Auth form to handle login/logout.
--
-- Consumes Events local to this module.  Mutates State local to this module.
-- Can only indirectly mutate global state by emitting global Events.
module UI.Auth
    ( State
    , init
    , foldp
    , view
    ) where

------------------------------------------------------------------------------
import Control.Applicative ( pure )
import Data.Either ( Either(Left, Right) )
import Data.Functor ( map )
import Data.Maybe ( Maybe(Just, Nothing) )
import Effects ( AppEffects
               , noEffects
               )
import Error ( Error
             , prettyError
             )
import Event ( Event( AuthFormEvent
                    , CloseAuthForm
                    )
             , AuthFormEvent(..)
             )
import Event as GlobalEvent
import HTTP as HTTP
import LocalId ( LocalId )
import Types ( User )
import Types as Types
import Prelude ( const
               , discard
               , ($)
               , (<<<)
               )
import Pux ( EffModel )
import Pux.DOM.Events ( onChange
                      , onClick
                      , targetValue
                      )
import Pux.DOM.HTML ( HTML )
import Text.Smolder.HTML as H
import Text.Smolder.HTML.Attributes as A
import Text.Smolder.Markup ( text
                           , (!)
                           , (#!)
                           )
------------------------------------------------------------------------------


------------------------------------------------------------------------------
-- |To make it clear we don't interpret global events.
type GlobalEvent = GlobalEvent.Event


------------------------------------------------------------------------------
-- |Current state of auth form.
newtype State = State { username :: String
                      , password :: String
                      , err      :: Maybe Error
                      , busy     :: Maybe String
                      }

init :: Maybe User -> State
init  Nothing        = State { username : ""
                             , password : ""
                             , err      : Nothing
                             , busy     : Nothing
                             }
init (Just u) = State { username : Types.username u
                      , password : Types.password u
                      , err      : Nothing
                      , busy     : Nothing
                      }

setUsername :: String -> State -> State
setUsername u (State s) = State s{username=u}

setPassword :: String -> State -> State
setPassword p (State s) = State s{password=p}

setBusy :: String -> State -> State
setBusy m (State s) = State s{busy=Just m}

notBusy :: State -> State
notBusy (State s) = State s{busy=Nothing}

setErr :: Error -> State -> State
setErr e (State s) = State s{err=Just e}

clearErr :: State -> State
clearErr (State s) = State s{err=Nothing}




------------------------------------------------------------------------------
-- |Emits global events.
foldp :: AuthFormEvent
      -> State
      -> LocalId (EffModel State GlobalEvent AppEffects)
foldp DoLogin st@(State s) = pure $ { state : clearErr $ setBusy "Logging in" st
                                    , effects : [login] 
                                    }
  where login = map (Just <<< AuthFormEvent <<< LoginResponse)
                    (HTTP.login s.username s.password)


foldp (LoginResponse (Right u)) s = pure $ { state  : s
                                           , effects: [ login
                                                      , close
                                                      ]
                                           }
  where login = pure $ Just $ GlobalEvent.Login u
        close = pure $ Just $ GlobalEvent.CloseAuthForm

foldp (LoginResponse (Left e)) s = noEffects $ notBusy $ setErr e s
foldp (SetUsername u) s = noEffects $ setUsername u s
foldp (SetPassword u) s = noEffects $ setPassword u s


------------------------------------------------------------------------------
-- |Emits global events.
view :: State -> HTML GlobalEvent
view (State s) =
  H.div do
    H.label (text "Username")
    H.input ! A.type' "text"
            ! A.value s.username
           #! onChange (AuthFormEvent <<< SetUsername <<< targetValue)
    H.label (text "Password")
    H.input ! A.type' "password"
            ! A.value s.password
           #! onChange (AuthFormEvent <<< SetPassword <<< targetValue)
    case s.busy of
        Nothing -> text ""
        Just m  -> text m
    case s.err of
        Nothing -> text ""
        Just e  -> text (prettyError e)
    H.button #! onClick (const $ AuthFormEvent DoLogin)
             $ text "Login"
    H.button #! onClick (const CloseAuthForm)
             $ text "Cancel"
