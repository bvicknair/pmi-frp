module UI.Util ( ctrlKey
               , empty
               ) where

------------------------------------------------------------------------------
import Control.Monad.Except ( runExcept )
import Control.Monad.Free ( liftF )
import Data.Either ( Either(Left, Right) )
import Data.Unit ( unit )
import DOM.Event.MouseEvent as MouseEvent
import Pux.DOM.Events ( DOMEvent )
import Text.Smolder.Markup ( Markup
                           , MarkupM(Empty)
                           )
------------------------------------------------------------------------------


------------------------------------------------------------------------------
-- |Is the Ctrl key held down during a MouseEvent?  If the DOMEvent is not a
-- MouseEvent, evaluates to false.
ctrlKey :: DOMEvent -> Boolean
ctrlKey domEvent = case runExcept (MouseEvent.eventToMouseEvent domEvent) of
                     Left _           -> false
                     Right mouseEvent -> MouseEvent.ctrlKey mouseEvent


empty :: forall e. Markup e
empty = liftF (Empty unit)
