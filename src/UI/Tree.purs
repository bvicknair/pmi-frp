-- |Tree view of project.
--
-- Each level of the tree shows all values of a particular attribute, for all
-- 'objects' that match the query induced by that tree-level's parent path.
--
-- A TreeSpec describes which attributes are at which level of the tree.
-- * Area
--   * System
--     * Material
--       * IsComplete
--
-- This might produce a UI view like this:
-- * CTG
--   * CCW
--     * CS
--       * Complete
--       * Incomplete
--     * SS
-- * HRSG1
--   * Pad 1
--   * Pad 2
--     * HDPE
--       * Complete
module UI.Tree
    ( State
    , init
    , selected
    , setSpec

    , foldp

    , view
    ) where

------------------------------------------------------------------------------
import Data.Foldable ( elem
                     , for_
                     , surround
                     )
import Data.Functor ( map )
import Data.List ( List(Cons, Nil) )
import Data.List as List
import Data.Maybe ( Maybe(Just, Nothing) )
import Data.Monoid ( mempty
                   , (<>)
                   )
import Data.Tuple ( Tuple(Tuple) )
import Effects ( AppEffects
               , noEffects
               )
import Event ( Event(TreeEvent)
             , TreeEvent( CollapsePath
                        , ExpandPath
                        , AlsoSelectPath
                        , OnlySelectPath
                        , UnselectPath
                        )
             )
import Index as Index
import LocalId ( LocalId )
import Prelude ( const
               , discard
               , ($)
               )
import Project ( Project )
import Project as Proj
import Pux ( EffModel )
import Pux.DOM.Events ( onClick )
import Pux.DOM.HTML ( HTML )
import Text.Smolder.HTML as H
import Text.Smolder.HTML.Attributes as A
import Text.Smolder.Markup ( text
                           , (!)
                           , (#!)
                           )
import UI.TreeTypes ( Attr
                    , AttrName
                    , AttrValue
                    , Path
                    , Spec
                    , inducedQuery
                    )
import UI.Util ( ctrlKey
               , empty
               )
------------------------------------------------------------------------------


------------------------------------------------------------------------------
-- |The state needed by a UI Tree component.
newtype State = State { spec     :: Maybe Spec
                      , expanded :: List Path
                      , selected :: List Path
                      }

spec :: State -> Maybe Spec
spec (State s) = s.spec

setSpec :: Spec -> State -> State
setSpec sp (State s) = State s{spec=Just sp}

expanded :: State -> List Path
expanded (State s) = s.expanded

expand :: Path -> State -> State
expand p (State s) = State s{expanded = List.Cons p s.expanded}

collapse :: Path -> State -> State
collapse p (State s) = State s{expanded = List.delete p s.expanded}

selected :: State -> List Path
selected (State s) = s.selected

alsoSelect :: Path -> State -> State
alsoSelect p (State s) = State s{selected=Cons p s.selected}

onlySelect :: Path -> State -> State
onlySelect p (State s) = State s{selected=Cons p Nil}

unselect :: Path -> State -> State
unselect p (State s) = State s{selected=List.delete p s.selected}



------------------------------------------------------------------------------
init :: State
init = State { spec     : Nothing
             , expanded : Nil
             , selected : Nil
             }


------------------------------------------------------------------------------
foldp :: TreeEvent
      -> State
      -> LocalId (EffModel State GlobalEvent AppEffects)
foldp (ExpandPath p) s = noEffects (expand p s)
foldp (CollapsePath p) s = noEffects (collapse p s)
foldp (AlsoSelectPath p) s = noEffects (alsoSelect p s)
foldp (OnlySelectPath p) s = noEffects (onlySelect p s)
foldp (UnselectPath p) s = noEffects (unselect p s)




------------------------------------------------------------------------------
-- |To disambiguate between possible future LocalEvents, that only this
-- component handles.
type GlobalEvent = Event


------------------------------------------------------------------------------
-- |To draw a tree view of a project, we need a Project, and the State of a
-- Tree component.
--
-- We'll query the Project's Index for attribute values etc.  We'll also query
-- the Index about PhantomPaths on the Project.
view :: Project -> State -> HTML GlobalEvent
view p s = case spec s of
             Nothing  -> viewEmptySpec
             Just spc -> viewWithSpec p s spc

------------------------------------------------------------------------------
viewEmptySpec :: HTML GlobalEvent
viewEmptySpec = text "Empty tree spec"

------------------------------------------------------------------------------
viewWithSpec :: Project -> State -> Spec -> HTML GlobalEvent
viewWithSpec p s sp = case mayForest of
                        Nothing -> text "No forest"
                        Just f  -> viewForest rootPath f
    where mayForest = forest sp rootPath (selected s) (expanded s) p
          rootPath = Nil


viewForest :: Parent -> Forest -> HTML GlobalEvent
viewForest parent (Forest attrName trees) = H.ul ! A.className "attr-forest"
                                                 $ treesView
  where treesView = case trees of
                      Nil -> text ("No objects with attribute " <> attrName)
                      _   -> for_ trees (treeView parent)


treeView :: Parent -> Selectable (Haunted Tree) -> HTML GlobalEvent
treeView parent node = viewNode parent isSel isPhan tree
    where tree = unHaunted $ unSelectable node
          isPhan = isPhantom $ unSelectable node
          isSel = isSelected node



viewNode :: Parent -> Boolean -> Boolean -> Tree -> HTML GlobalEvent
viewNode parent isSel isPhan tree = H.li ! A.className "attr-tree"
                                         $ do button
                                              label
                                              children
  where button = let click cnstr = onClick $ const $ TreeEvent $ cnstr path
                     exp = text "+ "
                     col = text "- "
                     cls = A.className "attr-node-toggle-expansion"
                 in case tree of
                      (Leaf _)       -> empty
                      (Unexpanded _) -> H.span #! click ExpandPath ! cls $ exp
                      (Expanded _ _) -> H.span #! click CollapsePath ! cls $ col
        label = let (Tuple _ val) = treeAttr tree
                    class_ = let cs = surround " " [ if isSel
                                                     then "selected"
                                                     else mempty
                                                   , if isPhan
                                                     then "phantom"
                                                     else mempty
                                                   , "attr-node"
                                                   ]
                             in A.className cs
                    click domEv = TreeEvent
                                $ if ctrlKey domEv
                                  then if isSel
                                       then UnselectPath path
                                       else AlsoSelectPath path
                                  else OnlySelectPath $ path
                    handleClick = onClick click
                in H.span #! handleClick
                           ! class_
                           $ text val
        children = case tree of
                     (Leaf _)              -> empty
                     (Unexpanded _)        -> empty
                     (Expanded _ Nothing)  -> empty
                     (Expanded _ (Just f)) -> viewForest path f
        path = pathAppend parent (treeAttr tree)
                            









-- ===========================================================================
-- Separated from UI layer, computing a tree from expanded trees and a spec.
-- ===========================================================================

-- TODO: Don't consider expanded/selected paths that don't have an
-- 'inducedSpec' equal to the current State.spec.

-- |Compute everything view will need to render.
forest :: Spec -> Parent -> Selected -> Expanded -> Project -> Maybe Forest
forest Nil _ _ _ _ = Nothing -- Empty spec
forest (Cons attrName specRest) parent selected' expanded' proj =
  Just $ Forest attrName trees
  where trees = map tree uniq
        uniq = let index   = Proj.index proj
                   q       = inducedQuery parent
                   results = Index.attrQuery q index
                   vals    = Index.selAttr attrName results index
               -- TODO: Must include phantom values in this result somehow.
               in List.fromFoldable vals
        tree value = maybeSelected
                   $ maybeHaunted
                   $ if isLeaf
                     then Leaf attr
                     else if isExpanded
                          then Expanded attr children
                          else Unexpanded attr
          where isExpanded = path `elem` expanded'
                isPhan = false -- TODO: Ask Project.Index.
                isSel = path `elem` selected'
                maybeHaunted = if isPhan then Phantom else Real
                maybeSelected = if isSel then Selected else Unselected
                isLeaf = List.null specRest -- We're @ end of Spec.
                path = pathAppend parent attr
                attr = Tuple attrName value
                children = if isExpanded
                           then forest specRest path selected' expanded' proj
                           else Nothing


------------------------------------------------------------------------------
pathAppend :: Path -> Attr -> Path
pathAppend = List.snoc


type Parent = Path
type Expanded = List Path
type Selected = List Path


data Forest = Forest AttrName (List (Selectable (Haunted Tree)))


-- |Difference between Leaf and Unexpanded is that Leaf is a Tree that cannot
-- have anything below it because it is at the last level of the Spec.
-- Unexpanded means it isn't at the last level of the Spec, but the user has
-- not expanded it.
data Tree = Leaf Attr
          | Unexpanded Attr
          | Expanded Attr (Maybe Forest)

data Selectable a = Selected   a
                  | Unselected a


treeAttr :: Tree -> Attr
treeAttr (Leaf a) = a
treeAttr (Unexpanded a) = a
treeAttr (Expanded a _) = a


isSelected :: forall a. Selectable a -> Boolean
isSelected (Selected _) = true
isSelected _ = false

unSelectable :: forall a. Selectable a -> a
unSelectable (Selected a) = a
unSelectable (Unselected a) = a


data Haunted a = Real a
               | Phantom a


unHaunted :: forall a. Haunted a -> a
unHaunted (Real a) = a
unHaunted (Phantom a) = a

isPhantom :: forall a. Haunted a -> Boolean
isPhantom (Phantom _) = true
isPhantom _ = false



-- Index will provide this eventually.
stubQuery :: Path -> List AttrValue
stubQuery _ = Nil


