-- |Originally created to handle a circular import problem.
module UI.TreeTypes
    ( AttrName
    , AttrValue
    , Attr
    , Path
    , Spec
    , inducedSpec
    , inducedQuery
    , inducedDisjunction
    ) where


------------------------------------------------------------------------------
import Data.Foldable ( foldl )
import Data.Functor ( map )
import Data.List ( List(Cons, Nil) )
import Data.Tuple ( Tuple(Tuple)
                  , fst
                  )
import Index ( AttrPredicate(AttrEqual)
             , AttrQuery
             , Query(All, And, IsTrue, None, Or)
             )
------------------------------------------------------------------------------

------------------------------------------------------------------------------
type AttrName  = String

------------------------------------------------------------------------------
-- |For now, all values are strings, and we may allow functions on strings to
-- attempt to interpret them as different types.
type AttrValue = String

------------------------------------------------------------------------------
type Attr = Tuple AttrName AttrValue

------------------------------------------------------------------------------
-- |An ordering of attribute names that define which attribute values should be
-- shown at each level of the tree.
type Spec = List AttrName

------------------------------------------------------------------------------
-- |A path in a tree, starting at the root.
type Path = List Attr


------------------------------------------------------------------------------
-- |A Path induces a Spec.  This is useful to check that a Path conforms to
-- some Spec, for example if a selected/expanded Path should even be considered
-- by the rendering function of this component, which if it doesn't have the
-- same Spec as the current Spec, it should not.
inducedSpec :: Path -> Spec
inducedSpec = map fst


------------------------------------------------------------------------------
-- |A Path induces an AttrQuery by taking the conjunction of all
-- attrName=attrValue predicates formed by each path segment.
inducedQuery :: Path -> AttrQuery
inducedQuery Nil = All
inducedQuery (Cons (Tuple name val) tail) = And (IsTrue (AttrEqual name val))
                                                (inducedQuery tail)


------------------------------------------------------------------------------
-- |Stick 'OR' between each Path's induced query.
inducedDisjunction :: List Path -> AttrQuery
inducedDisjunction = foldl step init 
  where init = None
        step query path = Or query (inducedQuery path)
