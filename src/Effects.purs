-- |Need to get this out of Foldp to avoid circular imports with Util.
module Effects ( AppEffects
               , noEffects
               ) where

------------------------------------------------------------------------------
import Control.Applicative ( pure )
import Data.UUID ( GENUUID )
import LocalId ( LocalId )
import Network.HTTP.Affjax ( AJAX )
import Prelude ( (<<<) )
import Pux ( EffModel )
import Pux as Pux
------------------------------------------------------------------------------


-- |Those effects used by this app beyond Pux.CoreEffects (CHANNEL and
-- EXCEPTION).
type AppEffects = ( ajax    :: AJAX
                  , uuid    :: GENUUID
                  )


------------------------------------------------------------------------------
-- |A wrapper around the usual Pux.noEffects that accounts for our LocalId
-- monad.  's' is the state type, 'e' is the event type.
noEffects :: forall e s. s -> LocalId (EffModel s e AppEffects)
noEffects = pure <<< Pux.noEffects
